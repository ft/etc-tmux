#!/bin/sh

NAME="spawn-session.sh"
TMUX_DIR="${TMUX_DIR:-${HOME}/etc/tmux}"

if [ $# -ne 1 ]; then
    printf 'usage: spawn-session.sh <session-name>\n'
    exit 1
fi

session="$1"
file="${TMUX_DIR}/session-${session}.conf"

if [ ! -r "${file}" ]; then
    printf 'Could not read session config: `%s'\''\n' "${file}"
    exit 1
fi

# Redirecting `has-session' currently blocks tmux forever.
#if tmux has-session -t "${session}" > /dev/null 2>&1; then
if tmux has-session -t "${session}"; then
    printf 'Session `%s'\'' already active in tmux. Doing nothing.\n' \
           "${session}"
    exit 23
fi

printf 'Starting tmux session `%s'\''...\n' "${session}"
tmux source-file "${file}"
exit $?
