#!/bin/sh

usage () {
    printf 'usage: xterm-font [FONT]\n'
}

wrap_tmux () {
    if test -n "$TMUX"; then
        printf '\ePtmux;\e'
    fi
    printf "$@"
    if test -n "$TMUX"; then
        printf '\e\\'
    fi
}

set_font () {
    font="$1"
    wrap_tmux '\e]50;%s\a' "$font"
}

awk_math () {
    a="$1"; op="$2"; b="$3"
    REPLY=$( awk 'BEGIN{printf("%.1f", '"$a"' '"$op"' '"$b"')}' )
}

current_font=$(tmux show-options -g -q -v @ft-xterm-font-name)
if test -z "$current_font"; then
    current_font="DejaVu Sans Mono"
    tmux set-option -g "@ft-xterm-font-name" "$current_font"
fi

default_size="14.5"
current_size=$(tmux show-options -g -q -v @ft-xterm-font-size)
if test -z "$current_size"; then
    current_size="$default_size"
    tmux set-option -g "@ft-xterm-font-size" "$current_size"
fi

if test "$#" -eq 0; then
    font="${current_font}-${current_size}"
elif test "$#" -eq 1; then
    case "$1" in
    default) size="$default_size"
             ;;
    [+-]) awk_math "$current_size" "$1" 0.5
          size="$REPLY"
          ;;
    [+-]*) size="${1#?}"
           op="${1%$size}"
           awk_math "$current_size" "$op" "$size"
           size="$REPLY"
           ;;
    *) size="$1"
       ;;
    esac
    tmux set-option -g "@ft-xterm-font-size" "$size"
    font="${current_font}-$size"
else
    usage
    exit 1
fi

set_font "$font"
